# Gnukey

## Introduction
Gnukey is a hardware security token.
This hardware token uses an STM32F103TB (ARM Cortex M-3 uCU) that can run Gnuk, Neug or u2f-token.

- Gnuk is a FOSS implementation of the GnuPG card protocol by Yutaka Niibe.
- Neug is a True Random Number Generator (TRNG) by Yutaka Niibe.
- [u2f-token](https://git.rnd2.org/erigas/u2f-token) is a universal second factor authenticator token by Sergei Glushchenko 

## PCB Assembly
The PCB is designed to be produced with a thicknes of 0.6mm and requires a case in order to fit inside the USB slot.
![final-pcb](https://git.rnd2.org/erigas/gnukey/raw/branch/slim/output/images/gnukey.jpg)

### BOM
Below is the bill of materials needed for soldering the gnukey pcb.
While there is one LED used, the table contains two (one red and one blue).
Similarly, for the LED resistor, depending on the resistance more or less current will pass through affecting the brightness.

The bill of materials can be found on [Octopart](https://octopart.com/bom-tool/hX9H97RJ).
Additionally, a 1-click-bom is in the root folder of the repository.

### Assembly
The PCB can be soldered in a home-lab using a hot-air soldering station and solder paste.
The image below shows both the bare PCB and the assembled one.
![pcbs](https://git.rnd2.org/erigas/gnukey/raw/branch/slim/output/images/gnukey_pcb.jpg)

And the following image shows the top and bottom view of the assembled PCB.
![top-bottom](https://git.rnd2.org/erigas/gnukey/raw/branch/slim/output/images/top-bottom.jpg)


### PCB case
As mentioned earlier, the PCB is designed to be used with a case.
The case designs can be found at [gnukey-ds](https://git.rnd2.org/erigas/gnukey-ds) repo.
Below is an image showing how the final Gnukey token will look like using the cases from the gnukey-ds repo.
![gnukey-case](https://git.rnd2.org/erigas/gnukey/raw/branch/slim/output/images/gnukey_case.jpg)

## Licence
Copyright Evangelos Rigas 2019.

This documentation describes Open Hardware and is licensed under the CERN OHL v.1.2 or later.
You may redistribute and modify this documentation under the terms of the CERN OHL v.1.2 (http://ohwr.org/cernohl).
This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
PARTICULAR PURPOSE. Please see the CERN OHL v.1.2 for applicable conditions.
